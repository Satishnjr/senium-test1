package Programs;

import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) {
		
			System.setProperty("webdriver.chrome.driver",
					"C:\\chromedriver_win32\\chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.get("http://www.monster.com");
			driver.manage().window().maximize();
			Set<String> windowsid = driver.getWindowHandles();
			System.out.println(windowsid);
			Object[] data = windowsid.toArray();
			System.out.println(data[0]);
			System.out.println(data[1]);
			driver.switchTo().window((String) data[0]);
			driver.close();

	}

}
