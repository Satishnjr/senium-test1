package CrossBrowsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class VerifyTitle {
	WebDriver driver;

	@Test
	@Parameters("browser")
	public void VerifyPageTitle(String browser) {
		if(browser.equalsIgnoreCase("chrome")){
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();

	}
		driver.manage().window().maximize();
		driver.get("http://www.Google.com");
		System.out.println(driver.getTitle());
	
	}
}
