import java.util.ArrayList;
import java.util.List;


public class DuplicateStrings {

	
		public static void main(String[] args){
			/*ArrayList<String> al = new ArrayList<String>();
		   // ArrayList<Object> al = new ArrayList<Object>();
		    al.add("Satish");
		    al.add("Chandu");
		    al.add("Raji");
		    al.add("Raji");
		    al.add("Satish");
		    al.add("Chandu");
		    al.add("Kishore");
		    
		    System.out.println("Before Duplicate Remove:"+al);
		    for(int i=0;i<al.size();i++){
		        for(int j=i+1;j<al.size();j++){
		            if(al.get(i).equals(al.get(j))){
		                al.remove(j);
		                j--;
		            }
		        }
		    }
		    System.out.println("After Removing duplicate:"+al);
		} */
			String[] aray = {"chandu","raji", "raji", "satish"};
	        
	        List<String> list = new ArrayList<>();
	        
	        for(String entry:aray){
	            if(!list.contains(entry)){
	                list.add(entry);
	            }
	        }
	        System.out.println(list);
	        System.out.println(list.toArray());
	}
}

